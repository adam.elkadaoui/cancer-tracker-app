function validatePatient(patient) {
  if (new Date(patient.dateOfBirth) > new Date()) {
    return "The patient's date of birth cannot be in the future";
  }
  if (patient.firstName.length < 2) {
    return "The patient's first name must contain at least 2 characters";
  }
  return null;
};

module.exports = validatePatient;