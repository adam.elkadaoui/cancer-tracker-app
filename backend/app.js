const sendEmailNotification = require('./notification');
const validatePatient = require('./patientUtils');
const cors = require('cors');

const express = require('express');
const bodyParser = require('body-parser');
const app = express();
app.use(cors());

// Parse requests of content-type - application/json
app.use(bodyParser.json());

// Define an array to store the patients
const patients = require("./mockData");

// GET all patients
app.get('/patients', (req, res) => {
  res.send(patients);
});

// GET patient by id
app.get('/patients/:id', (req, res) => {
  const patient = patients.find(p => p.id === parseInt(req.params.id));
  if (!patient) return res.status(404).send('Patient not found');
  res.send(patient);
});

// POST a new patient
app.post('/patients', (req, res) => {
  const patient = req.body;
  const validationError = validatePatient(patient);
  if (validationError) {
    return res.status(400).send(validationError);
  }
  patient.id = patients.length + 1;
  patients.push(patient);
  res.send(patient);
});

// PUT update an existing patient
app.put('/patients/:id', (req, res) => {
  const patient = patients.find(p => p.id === parseInt(req.params.id));
  if (!patient) return res.status(404).send('Patient not found');

  tempPatient = new patient();
  tempPatient.firstName = req.body.firstName;
  tempPatient.lastName = req.body.lastName;
  tempPatient.email = req.body.email;
  tempPatient.phone = req.body.phone;
  tempPatient.dateOfBirth = req.body.dateOfBirth;

  const validationError = validatePatient(tempPatient);
  if (validationError) {
    return res.status(400).send(validationError);
  }
  patient = tempPatient;
  sendEmailNotification(patient);

  res.send(patient);
});


// DELETE a patient
app.delete('/patients/:id', (req, res) => {
  const patient = patients.find(p => p.id === parseInt(req.params.id));
  if (!patient) return res.status(404).send('Patient not found');
  const index = patients.indexOf(patient);
  patients.splice(index, 1);
  res.send(patient);
});

module.exports = app;
// Start the server
const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`Listening on port ${port}...`));

