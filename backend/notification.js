function sendEmailNotification(patient) {
    const mailOptions = {
      from: 'your-email@gmail.com',
      to: 'recipient-email@example.com',
      subject: 'Patient Update Notification',
      text: `The patient with ID ${patient.id} has been updated: 
             Name: ${patient.firstName} ${patient.lastName}
             Email: ${patient.email}
             Phone: ${patient.phone}
             Date of birth: ${patient.dateOfBirth}`
    };
  
    /* 
    //The actual email senders do not need to be implemented)
    //todo: npm install nodemailer --save
    const nodemailer = require('nodemailer');
    const transporter = nodemailer.createTransport({
      service: 'gmail',
      auth: {
        user: 'your-email@gmail.com',
        pass: 'your-password'
      }
    });
    transporter.sendMail(mailOptions, function(error, info) {
      if (error) {
        console.log(error);
      } else {
        console.log('Email notification sent: ' + info.response);
      }
    });*/
    return mailOptions;
  }

module.exports = sendEmailNotification;