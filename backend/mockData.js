const patients = [
  {
    id: 1,
    firstName: 'John',
    lastName: 'Doe',
    email: 'johndoe@example.com',
    phone: '+352621234567',
    dateOfBirth: '1990-02-14'
  },
  {
    id: 2,
    firstName: 'Jane',
    lastName: 'Doe',
    email: 'janedoe@example.com',
    phone: '+352621234568',
    dateOfBirth: '1987-07-25'
  },
  {
    id: 3,
    firstName: 'Alice',
    lastName: 'Smith',
    email: 'alicesmith@example.com',
    phone: '+352621234569',
    dateOfBirth: '1975-05-12'
  },
  {
    id: 4,
    firstName: 'Bob',
    lastName: 'Johnson',
    email: 'bobjohnson@example.com',
    phone: '+352621234570',
    dateOfBirth: '1965-12-01'
  },
  {
    id: 5,
    firstName: 'Sarah',
    lastName: 'Jones',
    email: 'sarahjones@example.com',
    phone: '+352621234571',
    dateOfBirth: '1982-08-08'
  },
  {
    id: 6,
    firstName: 'David',
    lastName: 'Lee',
    email: 'davidlee@example.com',
    phone: '+352621234572',
    dateOfBirth: '1995-11-22'
  },
  {
    id: 7,
    firstName: 'Mary',
    lastName: 'Brown',
    email: 'marybrown@example.com',
    phone: '+352621234573',
    dateOfBirth: '1989-03-16'
  },
  {
    id: 8,
    firstName: 'Michael',
    lastName: 'Garcia',
    email: 'michaelgarcia@example.com',
    phone: '+352621234574',
    dateOfBirth: '1978-06-30'
  },
  {
    id: 9,
    firstName: 'Karen',
    lastName: 'Taylor',
    email: 'karentaylor@example.com',
    phone: '+352621234575',
    dateOfBirth: '1986-09-03'
  },
  {
    id: 10,
    firstName: 'Thomas',
    lastName: 'Wilson',
    email: 'thomaswilson@example.com',
    phone: '+352621234576',
    dateOfBirth: '1992-04-19'
  }
];
  
  module.exports = patients;
  