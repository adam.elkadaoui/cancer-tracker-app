const sendEmailNotification = require('../notification');

describe('sendEmailNotification', () => {
  it('should generate the correct email content', () => {
    const patient = {
      id: 123,
      firstName: 'John',
      lastName: 'Doe',
      email: 'johndoe@example.com',
      phone: '555-1234',
      dateOfBirth: '1980-01-01'
    };
    
    const mailOptions = sendEmailNotification(patient);
    
    expect(mailOptions.from).toBe('your-email@gmail.com');
    expect(mailOptions.to).toBe('recipient-email@example.com');
    expect(mailOptions.subject).toBe('Patient Update Notification');
    expect(mailOptions.text).toContain(`The patient with ID ${patient.id} has been updated`);
    expect(mailOptions.text).toContain(`Name: ${patient.firstName} ${patient.lastName}`);
    expect(mailOptions.text).toContain(`Email: ${patient.email}`);
    expect(mailOptions.text).toContain(`Phone: ${patient.phone}`);
    expect(mailOptions.text).toContain(`Date of birth: ${patient.dateOfBirth}`);
  });
});