const validatePatient = require('../patientUtils');

describe('validatePatient', () => {
  it('should return null if patient data is valid', () => {
    const patient = {
      id: 1,
      firstName: 'John',
      lastName: 'Doe',
      email: 'johndoe@example.com',
      phone: '555-555-5555',
      dateOfBirth: '1990-01-01'
    };

    expect(validatePatient(patient)).toBe(null);
  });

  it('should return a string if patient\'s date of birth is in the future', () => {
    const patient = {
      id: 1,
      firstName: 'John',
      lastName: 'Doe',
      email: 'johndoe@example.com',
      phone: '555-555-5555',
      dateOfBirth: '2090-01-01'
    };

    expect(validatePatient(patient)).toBe('The patient\'s date of birth cannot be in the future')
  });

  it('should return a string if patient\'s first name must contain at least 2 characters', () => {
    const patient = {
      id: 1,
      firstName: 'J',
      lastName: 'Doe',
      email: 'johndoe@example.com',
      phone: '555-555-5555',
      dateOfBirth: '1990-01-01'
    };

    expect(validatePatient(patient)).toBe('The patient\'s first name must contain at least 2 characters')
  });
});
