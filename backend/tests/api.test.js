const request = require("supertest");
const app = require("../app");
const patients = require("../mockData");

describe("Patients API", () => {
    
  describe("GET /patients", () => {
    it("should return all patients", async () => {
      const response = await request(app).get("/patients");
      expect(response.status).toBe(200);
      expect(response.body).toEqual(patients);
    });
  });

  describe("GET /patients/:id", () => {
    it("should return the patient with the specified ID", async () => {
      const id = 1; // we assume the first patient has an ID of 1
      const response = await request(app).get(`/patients/${id}`);
      expect(response.status).toBe(200);
      expect(response.body).toEqual(patients[0]);
    });

    it("should return a 404 status if the patient does not exist", async () => {
      const id = 999; // we assume there is no patient with an ID of 999
      const response = await request(app).get(`/patients/${id}`);
      expect(response.status).toBe(404);
    });
  });
});