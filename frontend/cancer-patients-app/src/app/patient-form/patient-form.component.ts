import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Patient } from '../patient';
import { PatientService } from '../patient.service';

@Component({
  selector: 'app-patient-form',
  templateUrl: './patient-form.component.html',
  styleUrls: ['./patient-form.component.css']
})
export class PatientFormComponent implements OnInit {
  @Input() patient: Patient;
  patientForm: FormGroup;
  isSubmitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private patientService: PatientService,
  ) { }

  ngOnInit() {
    this.patientForm = this.formBuilder.group({
      id: '',
      firstName: ['', [Validators.required, Validators.minLength(2)]],
      lastName: ['', Validators.required],
      email: ['', Validators.required],
      phone: ['', Validators.required],
      dateOfBirth: ['', Validators.required]
    });

    

    if (+this.route.snapshot.paramMap.get('id'))
    {
      const id = +this.route.snapshot.paramMap.get('id');
      this.patientService.getPatient(id).subscribe(patient => {
        this.patient = patient;
        this.patchFormValues();
      });
    }

    
  }

  patchFormValues() {
    if (this.patient) 
    {
      this.patientForm.patchValue({
        id: this.patient.id,
        firstName: this.patient.firstName,
        lastName: this.patient.lastName,
        email: this.patient.email,
        phone: this.patient.phone,
        dateOfBirth: this.patient.dateOfBirth
      });
    }
  }

  onSubmit() {
    this.isSubmitted = true;
    if (this.patientForm.invalid) {
      return;
    }

    const patient: Patient = this.patientForm.value;
    if (new Date(patient.dateOfBirth) > new Date()) {
      alert("The date of birth can't be in the future.");
      return;
    }

    if (patient.firstName.length < 2) {
      alert('The first name must contain at least 2 characters.');
      return;
    }

    if (this.patient) {
      this.patientService.updatePatient(patient)
        .subscribe(() => {
          alert('Patient updated successfully!');
        });
    } else {
      this.patientService.addPatient(patient)
        .subscribe(() => {
          alert('New patient added successfully!');
        });
    }
  }
}
