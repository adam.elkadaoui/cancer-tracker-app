import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Patient } from '../patient';
import { PatientService } from '../patient.service';

@Component({
  selector: 'app-patient-list',
  templateUrl: './patient-list.component.html',
  styleUrls: ['./patient-list.component.css']
})
export class PatientListComponent implements OnInit {
  patients: Patient[];

  constructor(private patientService: PatientService, private router: Router) {}

  ngOnInit() {
    this.getPatients();
  }

  getPatients(): void {
    this.patientService.getPatients().subscribe(patients => (this.patients = patients));
  }

  deletePatient(patient: Patient): void {
    this.patientService.deletePatient(patient.id).subscribe(() => {
      this.patients = this.patients.filter(p => p !== patient);
    });
  }

  editPatient(patient: Patient): void {
    const url = `/patients/${patient.id}/edit`;
    this.router.navigateByUrl(url);
  }
}
